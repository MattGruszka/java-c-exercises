package zad2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import zad1.Sentence;

public class Sentencer extends Sentence {
		
	String newName = "mySentence.txt";
	private final String path = "src\\zad2\\";
	
	public String writeSentence(String filename, String sentence) throws FileNotFoundException {
		
		File f2 = new File(this.path, this.newName);

				
		try {
			FileOutputStream fos =  new FileOutputStream(f2, true);
			PrintWriter pw = new PrintWriter(fos);
			pw.println(sentence);
			pw.close();
		
		} catch (FileNotFoundException e) {
			System.out.println("nie znaleziono pliku");
		}
		
		return sentence;
			
		
	}
}
