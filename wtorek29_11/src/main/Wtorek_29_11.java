package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import zad2.Sentencer;
import zad3.MoneyConverter;
import zad5.LengthChecker;

public class Wtorek_29_11 {
	    public static void main(String[] args) throws FileNotFoundException {
		
	    File f = new File("src\\main\\test.txt");
		Scanner sc = new Scanner(f);
		
		while(sc.hasNextLine()) {
			System.out.println(sc.nextLine());
		}
		
		File f2 = new File("src\\main\\test2.txt");
		FileOutputStream fos =  new FileOutputStream(f2, true);
		PrintWriter pw = new PrintWriter(fos);
		pw.println("przykladowy tekst");
		pw.close();
		 
	
		String filename = "test.txt";
	    
	    Sentencer ser = new Sentencer();
	    System.out.println(ser.writeSentence( filename , "dodatkowe zdanie"));
	    
	    MoneyConverter mc = new MoneyConverter();
	    System.out.println(mc.convert(100, "EUR"));
	    
	    LengthChecker lc = new LengthChecker();
	    lc.make("words.txt", 8);
	    
	    }
}