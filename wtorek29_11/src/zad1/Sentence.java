package zad1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Sentence {

	private final String path = "src\\zad1\\";
	
	String tmp = "";
	
	public String readSentence(String filename) throws FileNotFoundException {
	
		File f = new File(this.path + filename);
		try { 
			Scanner sc = new Scanner(f);
				while (sc.hasNextLine()) {
					tmp += sc.nextLine() + " ";
				}
			tmp = tmp.substring(0, 1).toUpperCase() + tmp.substring(1, tmp.length() - 1);
				if (tmp.substring(tmp.length() - 1).equals(".")) {
				} else { tmp += "."; } 
		} catch (FileNotFoundException e) {
			System.out.println("nie znaleziono podanego pliku");
		}
		return tmp;
	}
}
