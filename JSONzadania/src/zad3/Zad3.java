package zad3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class Zad3 {

	public Zad3() {
	};

	public void saveWordsInJASON() {

		File f = new File("src\\resources\\words.txt");
		String[] array = {};
		int i = 0;
		try {
			Scanner sc = new Scanner(f);
			JSONObject oj = new JSONObject();

			
			while (sc.hasNextLine()) {
					String a = sc.nextLine();
					try {
					String b = sc.nextLine();
					oj.put(a, b);
					} catch (NoSuchElementException e) {
						System.out.println("pominieto ostantni wyraz bo lista ma nieparzysta ilosc elementow");
					}
			}
			
			PrintWriter pw = new PrintWriter(new File("src\\resources\\words.json"));
			pw.print(oj);
			pw.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
