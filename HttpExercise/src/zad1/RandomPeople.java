package zad1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import main.HTTPConnector;

public class RandomPeople {
	

	private Map<String, String> params;
	private String url;
	
	public RandomPeople() {
		this.params = new HashMap<>();
		this.url = "http://randomuser.me/api/";
		params.put("nat", "US");
		params.put("results", "10");
	}
	
	public void run() throws IOException {
		System.out.println(this.encodeJSON(this.readJSON(this.getData())));
	}
	
	private String getData() throws IOException {
		HTTPConnector rs = new HTTPConnector();
		return rs.send("GET", params);
	}

	private Object readJSON(String json) {
		Object jsonObj = "";
		try {
			JSONParser parser = new JSONParser();
			jsonObj = parser.parse(json);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return jsonObj;
	}

	public Object encodeJSON(Object json) {
		String result = "";
		switch (json.getClass().getSimpleName()) {
		case "JSONArray":
			JSONArray array = (JSONArray) json;
			for (Object item : array) {
				if (item instanceof JSONArray || item instanceof JSONObject) {
					result += this.encodeJSON(item);
				} 
			}

		case "JSONObject":
			JSONObject object = (JSONObject) json;
			Iterator itrObj = object.keySet().iterator();
			while (itrObj.hasNext()) {
				Object k = itrObj.next();
				if (object.get(k) instanceof JSONArray || object.get(k) instanceof JSONObject) {
					result += this.encodeJSON(object.get(k));
				} else {
						if(object.containsKey("first") && object.containsKey("last")) {
						result += object.get("first") + " " + object.get("last") + " ";
						}
						if(object.containsKey("gender")) {
						result += (object.get("gender").equals("male")) ? "(M) - " : "(K) - ";
						}
						if(object.containsKey("email")) {
						result += object.get("email");
						}
				}
			}
			
		}

		return result;
	}

}
