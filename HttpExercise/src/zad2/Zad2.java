package zad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Zad2 {

	String hostName;
	String ua = "Matt/1.1";

	public Zad2() {
	}
	
	public void getHostName() {
		System.out.println("Write host name: ");
		Scanner sc2 = new Scanner(System.in);
		this.hostName = sc2.nextLine();
	}
	
	public void hostToLocation() {
		getHostName();
		System.out.println(giveLocation(takeIP()));
	}
	
	public String takeIP() {
		String url = "http://palo.ferajna.org/sda/host2ip.php?host=" + hostName;
		String tempString = "";
		String ipFromHost = "";
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.addRequestProperty("User-Agent", ua);
			con.setRequestMethod("GET");
			int code = con.getResponseCode();
			String currentLine = "";

			if (code == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((currentLine = br.readLine()) != null) {
					tempString += currentLine;
				}
				br.close();

				JSONObject jsonObj = new JSONObject();
				JSONParser parser = new JSONParser();

				jsonObj = (JSONObject) parser.parse(tempString);
				ipFromHost = (String) jsonObj.get("ip");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ipFromHost;
	}

	public String giveLocation(String ip) {

		String location = "";
		String url = "http://api.db-ip.com/v2/b620ee363136c97670e9054d4d2fa361c642c789/" + ip;
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestProperty("User-Agent", ua);
			con.setRequestMethod("GET");
			int code = con.getResponseCode();
			String currentLine;
			String tempString ="";
			Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				tempString += currentLine;
			}
			
			JSONObject jsonObj = new JSONObject();
			JSONParser parser = new JSONParser();
			jsonObj = (JSONObject) parser.parse(tempString);
			location = (String) jsonObj.get("continentName") + ", " + (String) jsonObj.get("countryName")
			 			+ ", " + (String) jsonObj.get("stateProv") + ", " + (String) jsonObj.get("city");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return location;
	}
}
