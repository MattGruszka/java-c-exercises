package main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import zad1.RandomPeople;
import zad2.Zad2;

public class Main {
	public static void main(String[] args) throws IOException {
		
		Zad2 z2 = new Zad2();
		z2.hostToLocation();
	}

	public String sendGET() throws IOException {
		
		String url = "http://palo.ferajna.org/sda/wojciu/json.php?login=test";
		
		String ua = "Pawel/1.0";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		con.setRequestProperty("User-Agent", ua);

		int responseCode = con.getResponseCode();
		String ret = "", currentLine = "";
		if (responseCode == 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			while((currentLine = br.readLine()) != null) {
				ret += currentLine;
			}
			br.close();
		} else {
			System.out.println("Code: " + responseCode);
		}
		return ret;
	}
	
	public String sendPOST() throws IOException {
		String url = "http://palo.ferajna.org/sda/wojciu/json.php";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		String ua = "Pawel/1.1";
		
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", ua);
		
		String myParams = "login=test&haslo=qwerty";
		con.setDoOutput(true);
		DataOutputStream dos = new DataOutputStream( con.getOutputStream() );
		dos.writeBytes(myParams);
		dos.flush();
		dos.close();
		
		String ret = "";
		int responseCode = con.getResponseCode();
		if(responseCode == 200) {
			Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
			
			while(sc.hasNextLine()) {
				ret += sc.nextLine();
			}
			sc.close();
		}
		
		return ret;
	}
	
}
