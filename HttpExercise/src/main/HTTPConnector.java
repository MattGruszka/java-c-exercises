package main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class HTTPConnector {

	private String url;
	private URL obj;
	private HttpURLConnection con;
	private String ua;
	
	public HTTPConnector() {
		this.url = "http://palo.ferajna.org/sda/wojciu/json.php";
		this.ua = "Mateusz/1.0";
	}
	
	public HTTPConnector(String url) {
		this.url = (url.substring(url.length()-1, url.length()).equals("/")) ? url.substring(url.length()-1, url.length()) : url;
		this.ua = "Mateusz/1.0";
	}
	
	public HTTPConnector(String url, String userAgent) {
		this.url = (url.substring(url.length()-1, url.length()).equals("/")) ? url.substring(url.length()-1, url.length()) : url;
		this.ua = userAgent;
	}
	
	private String sendPOST(String params) throws IOException {
			this.obj = new URL(this.url);
			this.con = (HttpURLConnection) obj.openConnection();
			this.con.setRequestMethod("POST");
			String ret = "";
			con.setDoOutput(true);
			DataOutputStream dos = new DataOutputStream(con.getOutputStream());
			dos.writeBytes(params);
			dos.flush();
			dos.close();
		
			int responseCode = con.getResponseCode();
			if(responseCode == 200) {
				Scanner scan = new Scanner(new InputStreamReader(con.getInputStream()));
				while(scan.hasNextLine()) {
					ret += scan.nextLine();
				}
				scan.close();
			}
			
			return ret;
				
	}
	
	private String sendGET(String params) throws IOException {
			this.obj = new URL(this.url + "?" + params);
			this.con = (HttpURLConnection) obj.openConnection();
			con.setRequestProperty("User Agent", ua);
			this.con.setRequestMethod("GET");
			int responseCode = this.con.getResponseCode();
			String ret = "";

			if(responseCode == 200) {
				String currentLine;
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while((currentLine = br.readLine()) != null) {
					ret += currentLine;
				}
			} else {
				System.out.println("Code: " + responseCode);
			}
		return ret;
		
	}
	
	public String send(String type, Map<String, String> params) throws IOException {
		String ret = "";
		String stringParams = this.convertMapToString(params);
		switch(type.toUpperCase()) {
		case "GET": ret = this.sendGET(stringParams); break;
		case "POST": ret = this.sendPOST(stringParams); break;
		}
		return ret;
	}
	
	private String convertMapToString(Map<String, String> params) {
		String result = "";
		Set<String> keys = params.keySet();
		for(String k : keys) {
			result += k + "=" + params.get(k) + "&";
		}
		return result.substring(0, result.length()-1);
		
	}

	
}	
