package zad3;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Zad3 {

	private String filename;
	private final String path = "src\\resources\\";

	private Zad3() {
	};

	public Zad3(String filename) {
		this.filename = filename;
	}

	public void getMinMaxStaffID() throws ParserConfigurationException, SAXException, IOException {

		File f = new File(path + filename);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("staff");
		Node tmpNmin = nList.item(0);
		Element tmpEmin = (Element) tmpNmin;
		Node tmpNmax = nList.item(0);
		Element tmpEmax = (Element) tmpNmax;

		int min = Integer.parseInt(tmpEmin.getAttribute("id"));
		int max = Integer.parseInt(tmpEmax.getAttribute("id"));
		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) n;
				if (Integer.parseInt(e.getAttribute("id")) < min) {
					min = Integer.parseInt(e.getAttribute("id"));
				}
				if (Integer.parseInt(e.getAttribute("id")) > max) {
					max = Integer.parseInt(e.getAttribute("id"));
				}
			}
		}
		System.out.println("min: " + min + " " + "max: " + max);
	}

}
