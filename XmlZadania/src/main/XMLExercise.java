package main;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLExercise {

	private final String path = "src\\resources\\";

	public void readXMLFile(String filename) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + filename);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);

		
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("staff");
		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;
				System.out.println("ID: " + elem.getAttribute("id"));
				System.out.println("FIRSTNAME: " + elem.getElementsByTagName("salary").item(0).getTextContent());
				System.out.println(" ==== ");

			}
		}

	}

	public void writeXMLFile(String filename) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("korzen");
		
		Element czlowiek = doc.createElement("czlowiek");
		czlowiek.setAttribute("id", "123");
		
		Element imie = doc.createElement("imie");
		imie.appendChild(doc.createTextNode("Pawel"));
		
		Element nazwisko = doc.createElement("nazwisko");
		nazwisko.setAttribute("value", "Testowy");
		
		czlowiek.appendChild(imie);
		czlowiek.appendChild(nazwisko);
		root.appendChild(czlowiek);
		doc.appendChild(root);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);
	}
}
