package zad4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class Zad4 {

	private String filename;
	private final String path = "src\\resources\\";
	
	
	private Zad4(){};
	public Zad4(String filename) {
		this.filename = filename;
	}
	
	
	public void writeStudents() throws ParserConfigurationException, SAXException, IOException, TransformerException {
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("ROOT");
		Element students = doc.createElement("STUDENTS");
		
		Element student1 = doc.createElement("STUDENT");
		Element name1 = doc.createElement("NAME");
		name1.appendChild(doc.createTextNode("John"));
		Element lastname1 = doc.createElement("LASTNAME");
		lastname1.appendChild(doc.createTextNode("Simple"));
		Element year1 = doc.createElement("YEAR");
		year1.appendChild(doc.createTextNode("3"));
		
		Element student2 = doc.createElement("STUDENT");
		Element name2 = doc.createElement("NAME");
		name2.appendChild(doc.createTextNode("Jane"));
		Element lastname2 = doc.createElement("LASTNAME");
		lastname2.appendChild(doc.createTextNode("doe"));
		Element year2 = doc.createElement("YEAR");
		year2.appendChild(doc.createTextNode("1"));
		
		student1.appendChild(year1);
		student1.appendChild(name1);
		student1.appendChild(lastname1);
		students.appendChild(student1);
		
		student2.appendChild(year2);
		student2.appendChild(name2);
		student2.appendChild(lastname2);
		students.appendChild(student2);
		
		root.appendChild(students);
		doc.appendChild(root);
		
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		
		StreamResult sr = new StreamResult(new File(this.path + filename));
		
		t.transform(source, sr);
	}
	
}
